package com.aeontronix.enhancedmule.installer;

public class MuleRuntimeInstallationException extends Exception {
    public MuleRuntimeInstallationException() {
    }

    public MuleRuntimeInstallationException(String message) {
        super(message);
    }

    public MuleRuntimeInstallationException(String message, Throwable cause) {
        super(message, cause);
    }

    public MuleRuntimeInstallationException(Throwable cause) {
        super(cause);
    }

    public MuleRuntimeInstallationException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
