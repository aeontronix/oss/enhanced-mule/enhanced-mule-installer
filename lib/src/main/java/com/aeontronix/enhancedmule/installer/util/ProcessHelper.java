package com.aeontronix.enhancedmule.installer.util;

import com.kloudtek.util.ProcessExecutionFailedException;
import com.kloudtek.util.ProcessExecutionResult;
import com.kloudtek.util.ThreadUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;

public class ProcessHelper {
    public static ProcessExecutionResult exec(File workDir, String... command) throws IOException, ProcessExecutionFailedException {
        StreamReader streamReader = new StreamReader();
        streamReader.start();
        final ProcessBuilder processBuilder = new ProcessBuilder(command);
        processBuilder.environment().put("MULE_HOME",workDir.getAbsolutePath());
        final ProcessBuilder builder = processBuilder
                .directory(workDir)
                .redirectErrorStream(true);
        Process process = builder.start();
        streamReader.setInputStream(process.getInputStream());

        try {
            process.waitFor();
        } catch (InterruptedException var4) {
        }
        streamReader.close();

        if (process.exitValue() != 0) {
            throw new ProcessExecutionFailedException(process, streamReader.getStdOut());
        } else {
            return new ProcessExecutionResult(process, streamReader.getStdOut());
        }
    }

    static class StreamReader extends Thread {
        private InputStream inputStream;
        private volatile ByteArrayOutputStream stdout = new ByteArrayOutputStream();
        private boolean running;

        StreamReader() {
        }

        public void setInputStream(InputStream inputStream) {
            this.inputStream = inputStream;
            synchronized (this) {
                running = true;
                notify();
            }
        }

        public String getStdOut() {
            return this.stdout.toString();
        }

        public void run() {
            try {
                synchronized (this) {
                    wait();
                }
                byte[] buf = new byte[1024];
                for (int i = this.inputStream.read(buf); this.inputStream.available() > 0 || i != -1; i = this.inputStream.read(buf)) {
                    this.stdout.write(buf, 0, i);
                }
            } catch (Exception var6) {
            } finally {
                synchronized (this) {
                    running = false;
                    notifyAll();
                }
            }
        }

        public void close() {
            try {
                try {
                    synchronized (this) {
                        while (running) {
                            wait(1000);
                        }
                    }
                } catch (InterruptedException e) {
                    //
                }
                inputStream.close();
                this.stdout.flush();
            } catch (Exception e) {
            } finally {
            }
        }
    }
}
