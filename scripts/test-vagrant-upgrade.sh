#!/bin/bash

cd ..
mvn package
mkdir -p target
cp ~/Downloads/mule-ee-distribution-standalone-4.3.0.zip target/mule.zip
vagrant ssh -c "rm -rf muleruntime-upg && java -cp /vagrant/cli/target/enhanced-mule-installer-cli-1.0-SNAPSHOT-nodeps.jar com.aeontronix.enhancedmule.installer.InstallerCLI -n dev -a /vagrant/target/mule.zip -u muleruntime -d muleruntime-upg"
