# Overview

This provides automation which automatically creates or upgrade mule runtimes

# Usage

In order to use, download the CLI jar file from the gitlab release, and run the following command for help

```
java -jar enhanced-mule-installer-cli-[version].jar -h
```

## Instances base directory

The installer uses a base directory where named instances will be created. This directory will default to C:\mule on windows
and /opt/mule on unix. You can also override this using the -b parameter. ie:

```
java -jar enhanced-mule-installer-cli-[version].jar -b /opt/muleruntimes -n dev01 -v 4.3.0
```

Additionally there is a special directory that will be automatically created named 'archive'. Any downloaded runtime zip
files will be stored there and reused. Also if a license.lic file is in that folder, it will automatically be used.

## License files

In order to register the license file, use the -l parameter. This will additionally put the license file in the archive
folder under the instance base directory

## Anypoint platform registration

In order to register your server with the anypoint platform, you will need to get a registration token from
[runtime manager](https://anypoint.mulesoft.com/cloudhub/#/console/home/servers), and pass it using the -r parameter

## Examples

### Create a 4.3.0 runtime named 'dev01', registering it on the anypoint platform with provided token

```
java -jar enhanced-mule-installer-cli-[version].jar -n dev01 -r 595cf45a-1152-4350-5356-234m234jsld---124245
```
