package com.aeontronix.enhancedmule.installer;

import com.kloudtek.util.logging.LoggingUtils;
import org.slf4j.Logger;
import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

import java.io.File;
import java.util.Arrays;
import java.util.concurrent.Callable;
import java.util.logging.Level;

import static org.slf4j.LoggerFactory.getLogger;

@Command(name = "muleinstall", mixinStandardHelpOptions = true, description = "Install a new mule runtime")
public class InstallerCLI implements Callable<Void> {
    private static final Logger logger = getLogger(InstallerCLI.class);
    @Option(names = {"-a", "--runtime-archive"}, description = "Runtime zip archive")
    private File runtimeArchive;
    @Option(names = {"-ad", "--archive-dir"}, description = "Archive directory (default to [basedir]/archive)")
    private File archiveDir;
    @Option(names = {"-b", "--basedir"}, description = "Base directory where all runtimes will be setup (default to C:\\mule on windows and /opt/mule on unix).")
    private File baseDir;
    @Option(names = {"-d", "--runtime-dir"}, description = "Mule runtime directory (defaults to [basedir]/[server name]).")
    private File directory;
    @Option(names = {"-n", "--name"}, description = "Server name (will be automatically picked up from old server when upgrading)")
    private String name;
    @Option(names = {"-r", "--registration-token"})
    private String regToken;
    @Option(names = {"-u","--upgrade"},description = "Upgrade a runtime")
    private boolean upgrade;
    @Option(names = {"-l","--license"},description = "License (defaults to [archive-dir]/licence.lic)")
    private File license;
    @Option(names = {"--force"},description = "Force installation")
    private boolean force;
    @Option(names = {"-v","--runtime-version"},description = "Runtime version")
    private String version;
    @Option(names = {"--debug"})
    private boolean debug;

    public static void main(String[] args) {
        final boolean debug = Arrays.asList(args).contains("--debug");
        LoggingUtils.setupSimpleLogging(debug ? Level.ALL : Level.INFO, true, true);
        int exitCode = new CommandLine(new InstallerCLI()).execute(args);
        System.exit(exitCode);
    }

    @Override
    public Void call() throws Exception {
        final MuleInstaller muleInstaller = new MuleInstaller(name, runtimeArchive, baseDir, directory, regToken);
        muleInstaller.setUpgrade(upgrade);
        muleInstaller.setForce(force);
        muleInstaller.setVersion(version);
        muleInstaller.setLicenseFile(license);
        muleInstaller.setArchiveDir(archiveDir);
        muleInstaller.install();
        return null;
    }
}
